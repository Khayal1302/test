package com.example1.demo1.service;

import com.example1.demo1.authentication.JwtTokenUtil;
import com.example1.demo1.dao.UserRepository;
import com.example1.demo1.entity.UserCredentials;
import com.example1.demo1.model.LoginRequest;
import com.example1.demo1.model.ResponseDTO;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.security.Key;
import java.util.*;

@Slf4j
@Service
public class UserService {


    private static final String SECRET_KEY = "ajksdfhakjshcfawyeribavysijkfyajshfdgasjdh";

    @Autowired
    UserRepository usersRepository;

    @Autowired
    private JwtUserDetailsService userService;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    //Getting all user records
    public List<ResponseDTO> getAllUsers() {
        log.info("Inside Method >>>>>> ");
        ResponseDTO responseDTO;
        List<ResponseDTO> response = new ArrayList<>();

        List<UserCredentials> userCredentials = (List<UserCredentials>) usersRepository.findAll();
        log.info("Database response>>>>>> " + userCredentials.toString());
        for (int i = 0; i < userCredentials.size(); i++) {

            responseDTO = new ResponseDTO();
            responseDTO.setUsername(userCredentials.get(i).getUsername());
            responseDTO.setFirstname(userCredentials.get(i).getFirstname());
            responseDTO.setLastname(userCredentials.get(i).getLastname());
            response.add(responseDTO);

        }
        return response;
    }

    //Getting a specific record by using findById()
    public UserCredentials getUsersById(int id) throws Exception {
        Optional<UserCredentials> optionalUserCredentials = usersRepository.findById(id);
        if (optionalUserCredentials.isPresent()) {
            return optionalUserCredentials.get();
        }
        throw new Exception("No User found with that ID");
    }

    //Create a new record
    public void saveOrUpdate(UserCredentials userCredentials) {
        log.info("Original password >> " + userCredentials.getPassword());
        userCredentials.setPassword(encrypt(userCredentials.getPassword()));

        /* BCryptPasswordEncoder encoder=new BCryptPasswordEncoder();
        log.info("BCrypt"+encoder.encode(userCredentials.getPassword()));*/

        log.info("Encrypted Password >> " + userCredentials.getPassword());
        usersRepository.save(userCredentials);
    }

    //Deleting a specific record
    public void delete(int id) {
        usersRepository.deleteById(id);
    }


    //Updating a record
    public void update(UserCredentials userCredentials, int id) {
        usersRepository.save(userCredentials);
    }

    private static String encrypt(final String data) {

        String secret = "442A472D4B614E64";
        byte[] decodedKey = Base64.getDecoder().decode(secret);

        try {
            Cipher cipher = Cipher.getInstance("AES");
            SecretKey originalKey = new SecretKeySpec(Arrays.copyOf(decodedKey, 16), "AES");
            cipher.init(Cipher.ENCRYPT_MODE, originalKey);
            byte[] cipherText = cipher.doFinal(data.getBytes("UTF-8"));
            return Base64.getEncoder().encodeToString(cipherText);
        } catch (Exception e) {
            throw new RuntimeException(
                    "Error occurred while encrypting data", e);
        }
    }

    public String loginUser(LoginRequest loginRequest) throws Exception {

        Optional<UserCredentials> optionalUserCredentials = usersRepository.findByUsername(loginRequest.getUsername());

        if (!optionalUserCredentials.isPresent()) {
            throw new Exception("User not Found");
        }
        UserCredentials userCredentials = optionalUserCredentials.get();
        String response;

        if (userCredentials.getPassword().equals(encrypt(loginRequest.getPassword()))) {
            final UserDetails userDetails = userService.loadUserByUsername(userCredentials.getUsername());
            long time=1000000000;
            response = createJWT(userDetails.getUsername(), "authorization", "khayal", time);

        } else {
            response = "unauthorized user";
        }
        return response;
    }

    public static String createJWT(String id, String subject, String issuer, long ttlMillis) {
    log.info(">>>>>>>>>");
        //The JWT signature algorithm we will be using to sign the token
        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

        long nowMillis = System.currentTimeMillis();
        Date now = new Date(nowMillis);

        //We will sign our JWT with our ApiKey secret
        byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(SECRET_KEY);
        Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());

        //Let's set the JWT Claims
        JwtBuilder builder = Jwts.builder().setId(id)
                .setIssuedAt(now)
                .setSubject(subject)
                .setIssuer(issuer)
                .signWith(signatureAlgorithm, signingKey);


        //if it has been specified, let's add the expiration
        if (ttlMillis >= 0) {
            long expMillis = nowMillis + ttlMillis;
            Date exp = new Date(expMillis);
            builder.setExpiration(exp);
        }

        //Builds the JWT and serializes it to a compact, URL-safe string
        return builder.compact();
    }

    public Claims verifyJwtToken(String jwt) {

        //This line will throw an exception if it is not a signed JWS (as expected)
        Claims claims = Jwts.parser()
                .setSigningKey(DatatypeConverter.parseBase64Binary(SECRET_KEY))
                .parseClaimsJws(jwt).getBody();
        return claims;
    }

}







