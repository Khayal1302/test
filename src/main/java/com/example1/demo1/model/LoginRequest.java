package com.example1.demo1.model;

import lombok.Data;

@Data
public class LoginRequest {

    private String username;
    private String password;
}
