package com.example1.demo1.model;

import lombok.Data;

@Data
public class ResponseDTO {

    public String firstname;
    public String lastname;
    private String username;
}
