package com.example1.demo1.dao;

import org.springframework.data.repository.CrudRepository;
import com.example1.demo1.entity.UserCredentials;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<UserCredentials, Integer> {
    Optional<UserCredentials> findByUsername(String username);
}
