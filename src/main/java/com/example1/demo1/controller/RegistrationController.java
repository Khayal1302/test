package com.example1.demo1.controller;

import com.example1.demo1.authentication.JwtTokenUtil;
import com.example1.demo1.entity.UserCredentials;
import com.example1.demo1.model.LoginRequest;
import com.example1.demo1.model.ResponseDTO;
import com.example1.demo1.service.JwtUserDetailsService;
import com.example1.demo1.service.UserService;
import io.jsonwebtoken.Claims;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

@Slf4j
@RestController

public class RegistrationController {

    @Autowired
    UserService userService;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private JwtUserDetailsService userService1;

    @GetMapping("/getAllUserDetails")
    private List<ResponseDTO> getAllUsers() {
        //log.info(">>>>>>>>>>>"+ userService.getAllUsers());
        return userService.getAllUsers();
    }

    //creating a get mapping that retrieves the details of a specific user
    @GetMapping("/getUserDetails/{userid}")
    private UserCredentials getUserCredentials(@PathVariable("userid") int userid) throws Exception {
        //log.info(">>>>>>>");
        return userService.getUsersById(userid);
    }

    //creating a delete mapping that deletes a specified user
    @DeleteMapping("/DeleteUser/{userid}")
    private void delete(@PathVariable("userid") int userid) {
        userService.delete(userid);
    }

    //creating post mapping that post the user details in the database
    @PostMapping("/registerUser")
    private UserCredentials saveOrUpdate(@RequestBody UserCredentials userCredentials) throws NoSuchAlgorithmException, IOException {
        userService.saveOrUpdate(userCredentials);
        //log.info(">>>>");
        return userCredentials;
    }

    //creating put mapping that updates the user details
    @PutMapping("/updateUser")
    private UserCredentials update(@RequestBody UserCredentials userCredentials) {
        userService.saveOrUpdate(userCredentials);
        //log.info(">>>>>>");
        return userCredentials;
    }

    //creating post mapping that show username and password
    @PostMapping("/users/login")
    public String loginUser(@RequestBody LoginRequest loginRequest) throws Exception {
        //log.info(">>>>>>");
        return userService.loginUser(loginRequest);
    }

    @RequestMapping("/hello")
    public String helloWorld() {
        return "Hello World";
    }

    @RequestMapping(value = "/checkToken", method = RequestMethod.POST)
    public Claims verifyJwt(@RequestParam("token")String token){
        return userService.verifyJwtToken(token);
    }
}





