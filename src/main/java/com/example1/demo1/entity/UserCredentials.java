package com.example1.demo1.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "userCredentials")
public class UserCredentials {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column
    public String firstname;
    @Column
    public String lastname;
    @Column
    private String username;
    @Column
    private String password;
}
